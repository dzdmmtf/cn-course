import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.net.DatagramPacket;

public abstract class TCPSocket implements CongestionWindowPlotter {
        public int base = 0;
        public int nextSeqNum = 0;
        public long winSize = 1;
        public int expectedSeqNum = 0;
        public int dupAckCount = 0;
        public int ssthresh = 200;
        public DatagramPacket[] receiveBuf = new DatagramPacket[10000];
        public DatagramPacket[] flowBuf = new DatagramPacket[1000];

        public TCPSocket(String ip, int port) throws Exception {
        }

        public abstract void send(ArrayList<String> packets) throws Exception;

        public final void sendAndLog(String pathToFile, ArrayList<String> packets) throws Exception {
                Path filePath = Paths.get(pathToFile);
                String fileName = filePath.getFileName().toString();
                long fileSize = Files.size(filePath);

                System.err.println(String.format("Starting to send file \"%s\" with size of %d[byte] ...", fileName,
                                fileSize));

                long sendBeginTime = System.currentTimeMillis();
                send(packets);

                System.err.println(String.format("File \"%s\" with size of %d[byte] sent in %d[ms].", fileName,
                                fileSize, System.currentTimeMillis() - sendBeginTime));
        }

        public abstract void receive(String pathToFile) throws Exception;

        public abstract void connect() throws Exception;

        public final void receiveAndLog(String pathToFile) throws Exception {
                Path filePath = Paths.get(pathToFile);
                String fileName = filePath.getFileName().toString();
                System.err.println(String.format("Starting to receive file \"%s\" ...", fileName));

                long receiveBeginTime = System.currentTimeMillis();
                receive(pathToFile);
                long receiveEndTime = System.currentTimeMillis();

                long fileSize = Files.size(filePath);
                System.err.println(String.format("File \"%s\" with size of %d[byte] received in %d[ms].", fileName,
                                fileSize, receiveEndTime - receiveBeginTime));
        }

        public abstract void close() throws Exception;
}