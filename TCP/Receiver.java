import java.io.IOException;
import java.net.DatagramPacket;

public class Receiver {
    public static void main(String[] args) throws Exception {
        TCPServerSocket tcpServerSocket = new TCPServerSocketImpl(9090);
        TCPSocket tcpSocket = tcpServerSocket.accept();
        tcpSocket.receive("output.txt");
        System.out.println("DONE !!!");
        tcpSocket.close();
        tcpServerSocket.close();
        System.exit(0);
    }
}