import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Sender {

    public static ArrayList<String> chunk = new ArrayList<String>();

    public void filePackets(String path) throws Exception {
        int fileLength;
        String fileData;
        int isFixed;
        fileData = new String(Files.readAllBytes(Paths.get(path)));
        fileLength = fileData.length();
        if (fileLength % 1000 == 0)
            isFixed = 0;
        else
            isFixed = 1;
        for (int i = 0; i < (fileLength /1000) + isFixed; i++) {
            String data;
            if (i == (fileLength / 1000) + isFixed - 1)
                data = fileData.substring(i * 1000, (i * 1000) + (fileLength % 1000));
            else
                data = fileData.substring(i * 1000, (i * 1000) + 1000);
            chunk.add(data);
        }
    }

    public static void main(String[] args) throws Exception {
        TCPSocket tcpSocket = new TCPSocketImpl("127.0.0.1", 4040);
        tcpSocket.connect();
        new Sender().filePackets("1MB.txt");
        tcpSocket.send(chunk);
        tcpSocket.close();
        tcpSocket.saveCongestionWindowPlot();
        System.out.println("Done!!!");
        System.exit(0);
    }
}