import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.net.InetAddress;
import java.net.DatagramPacket;
import java.io.IOException;
import java.util.Random;
import java.io.*;

public class TCPSocketImpl extends TCPSocket {

    Timer timer = new Timer();
    TimeCheck task = new TimeCheck();

    public EnhancedDatagramSocket currentSocket;
    static InetAddress inetAddress;

    public ArrayList<DatagramPacket> unAckBuffer = new ArrayList<>();
    public ArrayList<DatagramPacket> refusePacketBuffer = new ArrayList<>();
    public ArrayList<String> lossPackets = new ArrayList<>();

    FileWriter fw = new FileWriter("output.txt");
    BufferedWriter buffer = new BufferedWriter(fw);

    public String state = "slowStart";
    public String rcvSeq = "0";
    public int sentBeforeFR = 0;
    public int minWin = 0;
    public int lossCount = 0;
    public int rcvWin = 1;
    public int t = 30;

    public TCPSocketImpl(String ip, int port) throws Exception {
        super(ip, port);
        this.currentSocket = new EnhancedDatagramSocket(port);
        this.inetAddress = InetAddress.getByName("127.0.0.1");
        System.out.println("Port " + this.currentSocket.getLocalPort() + " binded to Client socket!");
    }

    public byte[] makeSendPayload(int seqNum, String data) {
        String payload = seqNum + "#" + data;
        return payload.getBytes(Charset.forName("UTF-8"));
    }

    public String[] tokenizeData(byte[] payload) {
        String data = new String(payload);
        String[] splitted = data.split("#");
        return splitted;
    }

    public void transmit(ArrayList<String> packets) {
        byte[] payload;
        while (nextSeqNum < (base + Math.min(winSize, rcvWin)) && packets.size() != nextSeqNum) {
            payload = makeSendPayload(nextSeqNum, packets.get(nextSeqNum));
            DatagramPacket packet = new DatagramPacket(payload, payload.length, this.inetAddress, 7070);
            try {
                this.currentSocket.send(packet);
            } catch (Exception e) {
                System.out.println("ERR in transmit");
            }

            System.out.println("SENT " + tokenizeData(packet.getData())[0] + " -> SIZE : " + packet.getData().length + "     WINDOW SIZE : " + winSize);
            this.unAckBuffer.add(packet);
            if (base == nextSeqNum) {
                try {
                    this.currentSocket.setSoTimeout(this.t);
                } catch (Exception e) {
                    System.out.println("ERR in set timer");
                }
            }
            nextSeqNum++;
        }
    }

    public void retransmit(ArrayList<String> packets, String rcvSeq) {
        System.out.println("retransmit segmet...");
        lossPackets.add(rcvSeq);
        lossCount++;
        for (int i = 0; i < unAckBuffer.size(); i++) {
            String[] split = tokenizeData(unAckBuffer.get(i).getData());
            if (!split[0].equals("~") && (Integer.parseInt(rcvSeq) + 1 == Integer.parseInt(split[0]))) {
                try {
                    this.currentSocket.send(unAckBuffer.get(i));
                    System.out.println("packet " + split[0] + " retransmited!");
                } catch (Exception e) {
                    System.out.println("ERR in retransmit");
                }
                break;
            }
            if (rcvSeq.equals("0")) {
                try {
                    if(!tokenizeData(unAckBuffer.get(0).getData())[0].equals("~")){
                        this.currentSocket.send(unAckBuffer.get(0));
                        System.out.println("packet 0 retransmited!");
                    }
                } catch (Exception e) {
                    System.out.println("ERR in retransmit");
                }
                break;
            }
        }
    }

    public void removeAckedPacket(String rcvSeq) {
        String[] splitted;
        byte[] data = new String("~####").getBytes(Charset.forName("UTF-8"));
        for (int i = 0; i < unAckBuffer.size(); i++) {
            splitted = tokenizeData(unAckBuffer.get(i).getData());
            if (!splitted[0].equals("~") && (Integer.parseInt(splitted[0]) <= Integer.parseInt(rcvSeq)))
                unAckBuffer.get(i).setData(data);
        }
    }

    public Boolean unAckIsEmpty() {
        String splitted;
        for (int i = 0; i < unAckBuffer.size(); i++) {
            splitted = new String(unAckBuffer.get(i).getData());
            if (!splitted.equals("~####"))
                return false;
        }
        return true;
    }

    public int unAckSize() {
        int cnt = 0;
        String splitted;
        for (int i = 0; i < unAckBuffer.size(); i++) {
            splitted = new String(unAckBuffer.get(i).getData());
            if (splitted.equals("~####"))
                cnt++;
        }
        return unAckBuffer.size() - cnt;
    }

    public void timeoutEvent(ArrayList<String> packets, String rcvSeq) {
        System.out.println("time out in receiving data");
        ssthresh = (int) (winSize / 2);
        this.onWindowChange();
        winSize = 1;
        this.onWindowChange();
        dupAckCount = 0;
        try {
            this.currentSocket.setSoTimeout(this.t);
        } catch (Exception e) {
            System.out.println("ERR in setting timer");
        }
        retransmit(packets, rcvSeq);
    }

    public String slowStart(ArrayList<String> packets) {
        byte[] data = new byte[1000];
        String[] tokenized;
        String isAck = "", isDupAck = "";
        DatagramPacket recvPacket = new DatagramPacket(data, data.length, this.inetAddress, 0);

        if (!unAckIsEmpty()) {
            try {
                this.currentSocket.receive(recvPacket);
                tokenized = tokenizeData(recvPacket.getData());
                System.out.println("GET: " + new String(recvPacket.getData()));
                if (tokenized.length != 1) {
                    rcvSeq   = tokenized[0];
                    isAck    = tokenized[1];
                    isDupAck = tokenized[2];
                    rcvWin   = Integer.parseInt(tokenized[3]);
                }
                if (isAck.equals("1")) {
                    System.out.println("-> ack received!\n\n");
                    removeAckedPacket(rcvSeq);
                    base = Integer.parseInt(rcvSeq) + 1;
                    winSize++;
                    this.onWindowChange();
                    dupAckCount = 0;
                    transmit(packets);
                }
                if (isDupAck.equals("1")) {
                    System.out.println("-> dupAck received!\n\n");
                    dupAckCount++;
                }
            } catch (IOException e) {
                timeoutEvent(packets, rcvSeq);
                this.state = "slowStart";
            } catch (Exception o) {
                System.out.println("ERROR");
            }
        }

        /*-----------INITIALIZE RETURN VALUE----------*/
        if (dupAckCount == 3) {
            ssthresh = (int) (winSize / 2);
            this.onWindowChange();
            winSize = ssthresh + 3;
            this.onWindowChange();
            this.state = "fastRecovery";
            retransmit(packets, rcvSeq);
        } else if (winSize >= ssthresh) {
            this.state = "congestionAvoidance";
        } else {
            this.state = "slowStart";
        }

        return this.state;
    }

    public String congestionAvoidance(ArrayList<String> packets) {
        byte[] data = new byte[1000];
        String[] tokenized, splitted;
        String isAck = "", isDupAck = "";
        DatagramPacket recvPacket = new DatagramPacket(data, data.length, this.inetAddress, 0);

        try {
            this.currentSocket.receive(recvPacket);
            tokenized = tokenizeData(recvPacket.getData());
            System.out.println("GET: " + new String(recvPacket.getData()));
            if (tokenized.length != 1) {
                rcvSeq   = tokenized[0];
                isAck    = tokenized[1];
                isDupAck = tokenized[2];
                this.rcvWin   = Integer.parseInt(tokenized[3]);
            }
            if (isAck.equals("1")) {
                System.out.println("-> ack received!\n\n");
                removeAckedPacket(rcvSeq);
                base = Integer.parseInt(rcvSeq) + 1;
                dupAckCount = 0;
                minWin++;
                if (minWin == winSize) {
                    minWin = 0;
                    winSize++;
                    this.onWindowChange();
                }
                transmit(packets);
                this.state = "congestionAvoidance";
            }

            if (isDupAck.equals("1")) {
                System.out.println("-> dupAck received!\n\n");
                dupAckCount++;
                this.state = "congestionAvoidance";
            }
        } catch (IOException i) {
            timeoutEvent(packets, rcvSeq);
            this.state = "slowStart";
        }
        if (dupAckCount == 3) {
            ssthresh = (int) (winSize / 2);
            this.onWindowChange();
            winSize = ssthresh + 3;
            this.onWindowChange();
            this.state = "fastRecovery";
            retransmit(packets, rcvSeq);
        }
        return this.state;
    }

    public String fastRecovery(ArrayList<String> packets) {
        byte[] data = new byte[1000];
        String[] tokenized, splitted;
        String isAck = "", isDupAck = "";
        DatagramPacket recvPacket = new DatagramPacket(data, data.length, this.inetAddress, 0);

        try {
            this.currentSocket.receive(recvPacket);
            tokenized = tokenizeData(recvPacket.getData());
            System.out.println("GET: " + new String(recvPacket.getData()));
            if (tokenized.length != 1) {
                rcvSeq   = tokenized[0];
                isAck    = tokenized[1];
                isDupAck = tokenized[2];
                rcvWin   = Integer.parseInt(tokenized[3]);
            }
            if (isAck.equals("1") && Integer.parseInt(rcvSeq) < sentBeforeFR) {
                System.out.println("-> ack received!\n\n");
                removeAckedPacket(rcvSeq);
                base = Integer.parseInt(rcvSeq) + 1;
                dupAckCount = 0;
                winSize = ssthresh;
                this.onWindowChange();
                retransmit(packets, rcvSeq);
                this.state = "fastRecovery";
            }
            if (isAck.equals("1") && Integer.parseInt(rcvSeq) >= sentBeforeFR) {
                removeAckedPacket(rcvSeq);
                dupAckCount = 0;
                winSize = ssthresh;
                this.onWindowChange();
                base = Integer.parseInt(rcvSeq) + 1;
                transmit(packets);
                this.state = "congestionAvoidance";
            }
            if (isDupAck.equals("1")) {
                System.out.println("-> dupAck received!\n\n");
                dupAckCount++;
                winSize++;
                this.onWindowChange();
                this.state = "fastRecovery";
                transmit(packets);
            }
        } catch (IOException i) {
            timeoutEvent(packets, rcvSeq);
            this.state = "slowStart";
        }
        return this.state;
    }

    @Override
    public void send(ArrayList<String> packets) {
        System.out.println("sender...");
        transmit(packets);
        while (true) {
            System.out.println("\n\n-----------------------------" + this.state + "-----------------------------"
                    + "dupAckCnt: " + dupAckCount + " winSize : " + winSize + " ssthresh : " + ssthresh + " next Seq : "
                    + nextSeqNum + "base: " + base + " rcvWin : " + rcvWin);
            switch (this.state) {
            case "slowStart":
                this.state = this.slowStart(packets);
                if (unAckSize() != 0)
                    sentBeforeFR = Integer.parseInt(tokenizeData(unAckBuffer.get(unAckBuffer.size() - 1).getData())[0]);
                else
                    sentBeforeFR = 0;
                break;
            case "congestionAvoidance":
                this.state = this.congestionAvoidance(packets);
                if (unAckSize() != 0)
                    sentBeforeFR = Integer.parseInt(tokenizeData(unAckBuffer.get(unAckBuffer.size() - 1).getData())[0]);
                else
                    sentBeforeFR = 0;
                break;
            case "fastRecovery":
                this.state = this.fastRecovery(packets);
                break;
            default:
                System.out.println("default state");
            }
            System.out.println("number of in-flight packets: " + unAckSize());
            System.out.println("loss: " + lossCount);
            System.out.println("loss packets: " + lossPackets);
            if (nextSeqNum == packets.size() && unAckIsEmpty())
                break;
        }
    }

    public static byte[] receiverMessage(int expectedseqnum, int ack, int dupAck) {
        Random rand = new Random();
        int rwnd = rand.nextInt(30) + 5 ;
        String data = expectedseqnum + "#" + ack + "#" + dupAck + "#" + rwnd + "#";
        return data.getBytes(Charset.forName("UTF-8"));
    }

    public Boolean hasSeqnum(DatagramPacket rcvpkt, int expectedseqnum) {
        String data = new String(rcvpkt.getData());
        if (data.length() != 1) {
            int seqnum = Integer.parseInt(data.split("#")[0]);
            if (expectedseqnum == seqnum) {
                this.receiveBuf[seqnum] = rcvpkt;
                for (int i = seqnum; i < this.receiveBuf.length; i++) {
                    if (receiveBuf[i] != null) {
                        this.expectedSeqNum++;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public void bufferData(DatagramPacket rcvpkt) throws Exception {
        String data = new String(rcvpkt.getData());
        int seqnum = Integer.parseInt(data.split("#")[0]);
        this.receiveBuf[seqnum] = rcvpkt;
    }

    public void writeIntoFile(DatagramPacket[] receiveBuf, int prevExpected, int currentExpected) throws Exception {
        try {
            for (int i = prevExpected; i < currentExpected; i++) {
                this.buffer.append(new String(tokenizeData(receiveBuf[i].getData())[1]).replaceAll("\0", ""));
                this.buffer.flush();
            }
            System.out.println("data was written in file successfully!");
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Error in writing into file!");
        }

    }

    @Override
    public void receive(String pathToFile) throws Exception {
        System.out.println("receiver...");
        while (true) {
            byte[] sndpayload = new byte[100];
            byte[] rcvpayload = new byte[1200];
            DatagramPacket rcvpkt = new DatagramPacket(rcvpayload, rcvpayload.length, this.inetAddress, 0);
            this.currentSocket.receive(rcvpkt);
            System.out.println("RECEIVE: " + new String(rcvpkt.getData()) + "   SIZE : " + rcvpkt.getData().length);
            int prevExpected = this.expectedSeqNum;
            if (Integer.parseInt(tokenizeData(rcvpkt.getData())[0]) < this.expectedSeqNum || this.expectedSeqNum == 0) {
                if (this.expectedSeqNum == 0) {
                    sndpayload = receiverMessage(this.expectedSeqNum, 1, 0);
                    hasSeqnum(rcvpkt, this.expectedSeqNum);
                    writeIntoFile(receiveBuf, 0, 1);
                }
                else {
                    sndpayload = receiverMessage(this.expectedSeqNum-1, 1, 0);
                }
            } else if (hasSeqnum(rcvpkt, this.expectedSeqNum)) {
                int currentExpected = this.expectedSeqNum;
                sndpayload = receiverMessage(this.expectedSeqNum - 1, 1, 0);
                writeIntoFile(receiveBuf, prevExpected, currentExpected);
            } else {
                sndpayload = receiverMessage(this.expectedSeqNum - 1, 0, 1);
                bufferData(rcvpkt);
            }
            DatagramPacket sndpkt = new DatagramPacket(sndpayload, sndpayload.length, this.inetAddress, 4040);
            this.currentSocket.send(sndpkt);
            System.out.println("SEND: " + new String(sndpkt.getData()) + "\n\n");
            int fileLength = Files.readAllBytes(Paths.get("1MB.txt")).length/1000;
            if (tokenizeData(sndpkt.getData())[0].equals(Integer.toString(fileLength))) {
                break;
            }
        }
    }

    @Override
    public void close() throws Exception {
        this.currentSocket.close();
    }

    @Override
    public long getSSThreshold() {
        return (long) ssthresh;
    }

    @Override
    public long getWindowSize() {
        return winSize;
    }

    public void printPayload(String IP, int srcPort, int SYN, int seqNum, int ackNum, String data) {
        System.out.println("-> IP : " + IP);
        System.out.println("-> Port : " + srcPort);
        System.out.println("-> SYN : " + SYN);
        System.out.println("-> Sequence number : " + seqNum);
        System.out.println("-> ACK number : " + ackNum);
        System.out.println("-> data : " + data);
    }

    public byte[] makePayload(int srcPort, int SYN, int seqNum, int ackNum, String data) {
        String IP = "127.0.0.1";
        String payload = IP + "#" + srcPort + "#" + SYN + "#" + seqNum + "#" + ackNum + "#" + data;
        printPayload(IP, srcPort, SYN, seqNum, ackNum, data);
        return payload.getBytes(Charset.forName("UTF-8"));
    }

    @Override
    public void connect() throws Exception {
        String[] tokenized;
        byte[] payload;
        byte[] data = new byte[128];
        Boolean synAckReceived = false;
        Random rand = new Random();
        int randomSeq = rand.nextInt(400) + 100;
        String ip = "", port = "", syn = "", seq = "", ack = "", packetData = "";
        boolean sendingSYN = true;
        DatagramPacket synAckPacket = new DatagramPacket(data, data.length, this.inetAddress, 0);

        /*----------------------------SEND SYN PACKET----------------------------*/
        while (sendingSYN) {
            System.out.println("Sending SYN packet to server...");
            payload = makePayload(4040, 1, randomSeq, -1, "");
            DatagramPacket synPacket = new DatagramPacket(payload, payload.length, this.inetAddress, 9090);
            this.currentSocket.send(synPacket);
            System.out.println("SYN packet sent to server!");
            this.currentSocket.setSoTimeout(3000);
            System.out.println("Set time.");

            /*----------------------------RECEIVE SYN-ACK PACKET----------------------------*/
            System.out.println("Receiving SYN-ACK from server...");
            while (!synAckReceived) {
                try {
                    this.currentSocket.receive(synAckPacket);
                    tokenized = tokenizeData(synAckPacket.getData());
                    if (tokenized.length != 1) {
                        ip = tokenized[0];
                        port = tokenized[1];
                        syn = tokenized[2];
                        seq = tokenized[3];
                        ack = tokenized[4];
                        packetData = tokenized[5];
                        if (ack.equals(Integer.toString(randomSeq + 1)) && syn.equals("1")) {
                            synAckReceived = true;
                            sendingSYN = false;
                            printPayload(ip, Integer.parseInt(port), Integer.parseInt(syn), Integer.parseInt(seq),
                                    Integer.parseInt(ack), packetData);
                        }
                    }
                } catch (IOException e) {
                    System.out.println("Timeout for receving SYN-ACK packet!");
                    break;
                }
            }
        }
        System.out.println("Received SYN-ACK packet from server!");

        /*----------------------------SEND ACK PACKET----------------------------*/
        System.out.println("Sending ACK packet to server...");
        payload = makePayload(4040, 0, Integer.parseInt(ack), Integer.parseInt(seq) + 1, "");
        DatagramPacket ackPacket = new DatagramPacket(payload, payload.length, inetAddress, 9090);
        this.currentSocket.send(ackPacket);
        System.out.println("ACK packet sent to server!");
    }
}