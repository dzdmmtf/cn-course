import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Random;

public class TCPServerSocketImpl extends TCPServerSocket {
    public EnhancedDatagramSocket socket;

    public TCPServerSocketImpl(int port) throws Exception {
        super(port);
        this.socket = new EnhancedDatagramSocket(port);
        System.out.println("Receiver is listening on " + this.socket.getLocalPort() + "!");
    }

    public String[] tokenizeData(byte[] b) {
        String data = new String(b);
        String[] splitted = data.split("#");
        return splitted;
    }

    public void printPayload(String IP, int srcPort, int SYN, int seqNum, int ackNum, String data){
        System.out.println("-> IP : " + IP);
        System.out.println("-> Port : " + srcPort);
        System.out.println("-> SYN : " + SYN );
        System.out.println("-> Sequence number : " + seqNum);
        System.out.println("-> ACK number : " + ackNum);
        System.out.println("-> data : " + data);
    }

    public byte[] makePayload(int srcPort, int syn, int seq, int ack, String data) {
        String packet = "127.0.0.1" + "#" + srcPort + "#" + syn + "#" + seq + "#" + ack + "#" + data;
        byte[] b = packet.getBytes(Charset.forName("UTF-8"));
        printPayload("127.0.0.1", srcPort, syn, seq, ack, data);
        return b;
    }

    @Override
    public TCPSocket accept() throws Exception {
        String[] splitted;
        byte[] data = new byte[128];
        DatagramPacket synPacket = new DatagramPacket(data, data.length, InetAddress.getByName("127.0.0.1"), 0);
        DatagramPacket synAckPacket = new DatagramPacket(data, data.length, InetAddress.getByName("127.0.0.1"), 0);
        String ip = "", port = "", prevSeq = "", seq = "", ack = "", SYN = "0", payload = "";
        Boolean synAckReceived = false, sendingSynAck = true;



        /*--------------------------receive SYN--------------------------*/
        while (SYN == "0") {
            this.socket.receive(synPacket);
            data = synPacket.getData();
            splitted = tokenizeData(data);
            if(splitted.length != 1) {
                ip  = splitted[0];
                port= splitted[1];
                SYN = splitted[2];
                prevSeq = splitted[3];
                ack = splitted[4];
                payload = splitted[5];
                printPayload(ip, Integer.parseInt(port), Integer.parseInt(SYN), Integer.parseInt(prevSeq), Integer.parseInt(ack), payload);
            }
        }

        System.out.println("SYN packet received from client!");

        /*--------------------------send SYNACK--------------------------*/
        while(sendingSynAck) {
            System.out.println("Sending SYN-ACK packet to client...");
            InetAddress ipAddress = InetAddress.getByName(ip);
            Random rand = new Random();
            int synAckSeq = rand.nextInt(400) + 100;
            data = makePayload(9090, 1, synAckSeq, Integer.parseInt(prevSeq) + 1, "");
            DatagramPacket synAck = new DatagramPacket(data, data.length, ipAddress, 4040);
            this.socket.send(synAck);
            System.out.println("SYN-ACK packet sent to client!");
            this.socket.setSoTimeout(3000);
            System.out.println("Set time.");


            /*--------------------------receive ACK--------------------------*/
            System.out.println("Receiving ACK from client...");
            while (!synAckReceived) {
                try {
                    this.socket.receive(synAckPacket);
                    data = synAckPacket.getData();
                    splitted = tokenizeData(data);
                    if (splitted.length != 1) {
                        ip  = splitted[0];
                        port= splitted[1];
                        SYN = splitted[2];
                        seq = splitted[3];
                        ack = splitted[4];
                        payload = splitted[5];
                        if (seq.equals(Integer.toString(Integer.parseInt(prevSeq) + 1)) && ack.equals(Integer.toString(synAckSeq + 1))) {
                            synAckReceived = true;
                            sendingSynAck = false;
                            printPayload(ip, Integer.parseInt(port), Integer.parseInt(SYN), Integer.parseInt(seq), Integer.parseInt(ack), payload);
                        }
                    }
                }
                catch (SocketTimeoutException e) {
                    System.out.println("Timeout for receving ACK packet!");
                    break;
                }
            }
        }

        System.out.println("ACK packet received from client!");
        System.out.println("handshake completed!!!");

        return new TCPSocketImpl("127.0.0.1", 7070);
    }


    @Override
    public void close() throws Exception {
        this.socket.close();
    }
}
