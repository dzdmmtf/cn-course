import socket
BUFFER_SIZE = 1024*64

class Link:
    
    def __init__(self, ip, port):
        self.mySocket = self.initSocket()
        self.bindSocket(ip, port)
        

    def initSocket(self):
        s = socket.socket(family= socket.AF_INET, type = socket.SOCK_DGRAM)
        return s
    
    def bindSocket(self, ip, port):
        self.mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.mySocket.bind((ip, port))

    
    def sendSocket(self,data, ip, port):
        self.mySocket.sendto(data, (ip, port))
    
    def rcvSocket(self):
        pkt, addr = self.mySocket.recvfrom(BUFFER_SIZE)
        return pkt