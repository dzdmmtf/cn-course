import sys
import threading
import socket
import time
import random
from link import Link

portVirtualIp = {}
check = 0
q = False
forTable = []
downStatus = []
otherDownStatus = []


def parseLnx(fileName):
    file = open(fileName, "r")
    data = file.read().split('\n')
    dstIps = []
    dstPorts = []
    dstInterfaces = []
    myInterfaces = []
    for i in range(len(data)):
        if(data[i] != ""):
            line = data[i].split(' ')
            if(i == 0):
                myIp = line[0]
                myPort = line[1]
            else:
                dstIps.append(line[0])
                dstPorts.append(line[1])
                dstInterfaces.append(line[3])
                myInterfaces.append(line[2])
    return myIp, myPort, dstIps, dstPorts, dstInterfaces, myInterfaces


def existsDst(dst, cost):
    for i in range (len(forTable)):
        if(forTable[i][2] == dst):
            if(cost >= forTable[i][0]):
                return True
            forTable.pop(i)
            return False
    return False


def forwardPacket(s, packet, dstInterfaces, dstPorts):
    flag = False
    data = packet.split('#')
    nextRouterPort = data[1]
    finalVirtualIp = data[2]
    payload = data[3]

    for i in range(len(forTable)):
        if(forTable[i][2] == finalVirtualIp):
            nextRouterVirtualIp = forTable[i][3].split('->')[1]
            break
    for i in range(len(dstInterfaces)):
        if(dstInterfaces[i] == nextRouterVirtualIp):
            nextRouterPort = dstPorts[i]
            break
    payload = payload.split('&')
    payload = ' '.join(payload)
    print(payload + ' sending to ' + nextRouterVirtualIp)
    data =  "0" + "#" + nextRouterPort + "#" + finalVirtualIp + "#" + payload
    s.sendSocket(data, "127.0.0.1", int(nextRouterPort))
    # s.sendto(data, ("127.0.0.1", int(nextRouterPort)))


def delDownLinks(downLinks):
    for i in range(len(forTable)-1, -1, -1):
        path = forTable[i][3].split('->')
        for j in range(len(path)-1):
            for k in range(0, len(downLinks), 2):
                if((path[j] == downLinks[k] and path[j+1] == downLinks[k+1]) or (path[j] == downLinks[k+1] and path[j+1] == downLinks[k])): 
                    del forTable[i]


def checkDown(content):
    path = content.split('->')
    for j in range(len(path)-1):
        for k in range(0, len(otherDownStatus), 2):
            if((path[j] == otherDownStatus[k] and path[j+1] == otherDownStatus[k+1]) or (path[j] == otherDownStatus[k+1] and path[j+1] == otherDownStatus[k])): 
                return True
    return False


def updateForwardingTable(payload, interface, myInterfaces, dstInterfaces):
    for i in range(len(dstInterfaces)):
        if(dstInterfaces[i] == interface):
            myInterface = myInterfaces[i]
            break

    delDownLinks(otherDownStatus)

    splitted = payload.split('***')
    for i in range(len(splitted)):
        tmp = splitted[i].split('~')
        t = []
        t.append(int(tmp[0]) + 1)
        t.append(myInterface)
        t.append(tmp[2])
        condition = (interface == tmp[3]) if ('->' not in tmp[3]) else (interface == tmp[3].split('->')[0])
        content = (myInterface + "->" + tmp[3]) if (condition) else (myInterface + "->" + interface + "->" + tmp[3])

        t.append(content)
        if((not existsDst(tmp[2], int(tmp[0]) + 1)) and (not checkDown(content))):
            forTable.append(t)


def updateDownPath(downPath, myInterfaces, dstInterfaces):
    splitted = downPath.split('^')
    if(splitted[0] == ''):
        splitted = []

    global otherDownStatus
    otherDownStatus = splitted
    for i in range(0, len(downStatus), 2):
        found = False
        for j in range(0, len(otherDownStatus), 2):
            if((otherDownStatus[j] == downStatus[i] and otherDownStatus[j+1] == downStatus[i+1]) or (otherDownStatus[j] == downStatus[i+1] and otherDownStatus[j+1] == downStatus[i])):
                found = True
                continue
        if(not found):
            global otherDownStatus
            otherDownStatus.append(downStatus[i])
            otherDownStatus.append(downStatus[i+1])

    if(len(otherDownStatus) != 0):
        upStatus = []
        for i in range(len(myInterfaces)):
            found = False
            for j in range(0, len(downStatus), 2):
                if(myInterfaces[i] == downStatus[j]):
                    found = True
            if(not found):
                upStatus.append(myInterfaces[i])
                upStatus.append(dstInterfaces[i])

        for i in range(len(otherDownStatus)-2, -1, -2):
            for j in range(0, len(upStatus)-1, 2):
                if(upStatus[j] == otherDownStatus[i] and upStatus[j+1] == otherDownStatus[i+1]):
                    global otherDownStatus
                    del otherDownStatus[i]
                    del otherDownStatus[i]
                    break
      

def process(**args):
    socket = args['s']
    protocolNum = args['p']
    data = args['d']
    myInterfaces = args['mi']
    dstInterfaces = args['di']
    dstPorts = args['dp']
    pkt = args['pkt']

    finish = False
    if(protocolNum == "0"):
        finalVirtualIp = data[2]
        payload = data[3]
        for i in range(len(myInterfaces)):
            if(myInterfaces[i] == finalVirtualIp):
                finish = True
        if(finish):
            print('packet received to destination')
            payload = payload.split('&')
            payload = ' '.join(payload)
            print('payload is: ' + payload)
        else:
            forwardPacket(socket, pkt, dstInterfaces, dstPorts)
    else:
        srcIp     = data[1]
        srcPort   = data[2]
        dstIp     = data[3]
        dstPort   = data[4]
        interface = data[5]
        payload   = data[6]
        downPath  = data[7]
        updateDownPath(downPath, myInterfaces, dstInterfaces)
        updateForwardingTable(payload, interface, myInterfaces, dstInterfaces)


def register_handler(process, **args):
    process(s=args['s'],p=args['p'],d=args['d'],mi=args['mi'],di=args['di'],dp=args['dp'],pkt=args['pkt'])


def listen(s, myIp, myPort, myInterfaces, dstInterfaces, dstPorts):
    while(True):
        pkt = s.rcvSocket()
        # pkt, addr = s.recvfrom(BUFFER_SIZE)
        data = pkt.split('#')
        protocolNum = data[0]
        register_handler(process, s=s, p=protocolNum, d=data, mi=myInterfaces, di=dstInterfaces, dp=dstPorts, pkt=pkt)
        if(q):
        	break


def convertToString(myIp, myPort, dstIp, dstPort, dstPorts, myInterfaces):
    downPath = ""
    t = ""
    delimiter = '^'

    for i in range(len(dstPorts)):
        if(dstPorts[i] == dstPort):
            sendingInterface = myInterfaces[i]
            break
    data = "200" + "#" + "127.0.0.1" + "#" + myPort + "#" + "127.0.0.1" + "#" + dstPort + "#" + sendingInterface + "#"
   
    for i in range(len(forTable)):
        t += str(forTable[i][0]) + "~" + forTable[i][1] + "~" + forTable[i][2] + "~" + forTable[i][3]
        if(i != len(forTable)-1):
            t += "***"

    downPath = delimiter.join(otherDownStatus)
    return data + t + "#" + downPath


def sendForwardingTable(s, dstIps, dstPorts, myIp, myPort, myInterfaces):
    while(True):
        for i in range(len(dstPorts)):
            forTablePkt = convertToString(myIp, myPort, dstIps[i], dstPorts[i], dstPorts, myInterfaces)
            s.sendSocket(forTablePkt, "127.0.0.1", int(dstPorts[i]))
            # socket.sendto(forTablePkt, ("127.0.0.1", int(dstPorts[i])))
        t = random.randint(1,4)
        time.sleep(int(t))
        if(q):
        	break


def interfaces(dstInterfaces, myInterfaces):
    id = 0
    print('id               src                   dst')
    for i in range(len(myInterfaces)):
        print(" " + str(id) + "            " + myInterfaces[i] + "          " + dstInterfaces[i])
        id += 1


def routes():
    print('cost              src                  dst                  path')
    for i in range(len(forTable)):
        print(" " + str(forTable[i][0]) + "            " + forTable[i][1] + "          " + forTable[i][2] + "          " + forTable[i][3])


def traceroute(dst):
    trace = []
    for i in range(len(forTable)):
        if(forTable[i][2] == dst):
            print('Traceroute from ' + forTable[i][1] + ' to ' + dst)
            trace = forTable[i][3].split('->')
            for a in trace:
                print(a)
            break  

    if(trace == []):
        print('There is no route from ' + forTable[i][1] + ' to ' + dst)


def makeData(finalVirtualIp, payload, dstPorts, myPort, myInterfaces, dstInterfaces):

    for i in range(len(forTable)):
        if(forTable[i][2] == finalVirtualIp):
            srcVirtualIp = forTable[i][1]
            break
    for i in range(len(myInterfaces)):
        if(myInterfaces[i] == srcVirtualIp):
            nextRouterPort = dstPorts[i]
            break
    for i in range(len(myInterfaces)):
        if(myInterfaces[i] == finalVirtualIp):
            nextRouterPort = myPort
    payload = '&'.join(payload)
    data = "0" + "#" + nextRouterPort + "#" + finalVirtualIp + "#" + payload
    return data, nextRouterPort


def down(index, myInterfaces, dstInterfaces, qStat):
	for i in range(len(downStatus)):
		if(downStatus[i] == myInterfaces[index]):
			if(not qStat):
				print("already down!")
			return
	downStatus.append(myInterfaces[index])    #head
	downStatus.append(dstInterfaces[index])   #tail

	delDownLinks(downStatus)


def up(index, myInterfaces):
    found = False
    for i in range(len(downStatus)-1): 
        if(downStatus[i] == myInterfaces[index]):
            found = True
            del downStatus[i]
            del downStatus[i]
            break

    if(not found):
        print("already up!")


def quit(myInterfaces, dstInterfaces, listenThread, sendForwardingThread):
	for j in range(len(myInterfaces)):
		down(j, myInterfaces, dstInterfaces, True)
		time.sleep(5)
	
	time.sleep(10)
	global q
	q = True
	listenThread.join()
	sendForwardingThread.join()
    

def command(s, listenThread, sendForwardingThread, dstInterfaces, myInterfaces, dstPorts, myPort):
    while(True):  
        cmd = raw_input('->')
        if(cmd == "interfaces"):
            interfaces(dstInterfaces, myInterfaces)
        elif(cmd == "routes"):
            routes()
        elif(cmd == 'q'):
        	quit(myInterfaces, dstInterfaces, listenThread, sendForwardingThread)
        	break
        elif(cmd[0:2] == "up"):
                up(int(cmd[3:len(cmd)]), myInterfaces)
        elif(len(cmd) > 4):
            if(cmd[0:4] == "down"):
                down(int(cmd[5:len(cmd)]), myInterfaces, dstInterfaces, False)
            elif(cmd[0:4] == "send"):
                cmd = cmd.split(' ')
                length = len(str(cmd[3:]))
                payload = cmd[3:]
                count = 0
                while(length > 0):
                    data, nextRouterPort = makeData(cmd[1], payload[count*1200:min((count+1)*1200, len(payload))], dstPorts, myPort, myInterfaces, dstInterfaces)
                    s.sendSocket(data, "127.0.0.1", int(nextRouterPort))
                    # s.sendto(data, ("127.0.0.1", int(nextRouterPort)))
                    length -= 1200
                    count  += 1
            elif(cmd[0:10] == "traceroute"):
                dst = cmd[11:]
                traceroute(dst)
            else:
                print("Unknown command!") 
        else:
            print("Unknown command!")
    print('bye!')


def initForTable(myInterfaces):
    for i in range(len(myInterfaces)):
        tmp = []
        tmp.append(0)
        tmp.append(myInterfaces[i])
        tmp.append(myInterfaces[i])
        tmp.append(myInterfaces[i])
        forTable.append(tmp)



if __name__ == "__main__":
    myIp, myPort, dstIps, dstPorts, dstInterfaces, myInterfaces = parseLnx(sys.argv[1])
    initForTable(myInterfaces)

    s = Link("127.0.0.1", int(myPort))

    sendForwardingThread = threading.Thread(target=sendForwardingTable, args=(s, dstIps, dstPorts, myIp, myPort, myInterfaces))
    listenThread = threading.Thread(target=listen, args=(s, myIp, myPort, myInterfaces, dstInterfaces, dstPorts))
    
    listenThread.start()
    sendForwardingThread.start()

    command(s, listenThread, sendForwardingThread, dstInterfaces, myInterfaces, dstPorts, myPort)