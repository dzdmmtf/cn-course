import json
import socket
from thread import *
from threading import Thread
import threading
import sys
import datetime
import base64


isEnable  = False
totalUsed = 0
cacheDir = []
cacheRes = []

def parseConfig():
    with open('config.json') as json_file:
        data = json.load(json_file)
        port = data['port']
        isEnable = data['logging']['enable']
        logFile  = data['logging']['logFile']
        privacy = data['privacy']
        users = data['accounting']['users']
        restrictionEnable = data['restriction']['enable']
        targets = data['restriction']['targets']
        cacheEnable = data['caching']['enable']
        cacheSize = data['caching']['size']
        isInjection = data['HTTPInjection']['enable']
        navBar = data['HTTPInjection']['post']['body']
        print navBar

    return port, isEnable, logFile ,privacy, users, restrictionEnable, targets, cacheEnable, cacheSize, isInjection, navBar


def getHostName(hostName):
	print hostName
	return socket.gethostbyname(hostName[:len(hostName)-2])    


def writeIntoFile(info):
    if(info == ""):
        info= "empty data"
    date = datetime.datetime.now()
    dateTime = "[" + str(date) + "] :"
    if(isEnable):
        with open("proxy.log", "a") as log_file:
            log_file.write(dateTime + info +  '\n')
            log_file.close()


def createServerSocket(ip,hostName):
    try:
        toServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        writeIntoFile("server socket created.")
    except socket.error, msg:
        sys.stderr.write("[ERROR] in create server socket %s\n" % msg[1])
        sys.exit(1)

    serverIp = getHostName(hostName)
   
    toServerSocket.connect((serverIp ,80))
    writeIntoFile("connected to sever.")
    return toServerSocket


def changeUserAgent(data, privacy):
    tmp = data
    enable = privacy['enable']
    userAgent = privacy['userAgent']
    for i in range(len(tmp)):
        if(tmp[i][0:10] == "User-Agent" and enable == True):
            tmp[i] = "User-Agent: " + userAgent
        if(tmp[i][0:17] == "Accept-Encoding: "):
        	tmp[i] = "Accept-Encoding: identify" + '\r'
    return tmp


def checkRestriction(hostName, restrictionEnable, targets):
    filterUser = False

    for target in targets:
        if(restrictionEnable == True):
            targetHost = ""
            if(hostName[0:3] == "www"):
                hostName = hostName[4:]
            if(target['URL'][0:3] == "www"):
                targetHost = target['URL'][4:]
            else:
                targetHost = target['URL']
            targetHost = targetHost.encode('utf-8')
            if(hostName[0:len(hostName)-2] == targetHost):
                filterUser = True
    return filterUser


def changeData(inputData, privacy, restrictionEnable, targets):
    getLine = ""
    data, changeUserData = "", ""
    hostName = ""
    slashCnt = 0
    cnt = 0

    message = inputData.decode("utf-8")
    message = message.split('\n')

    for char in message[0]:
        if(char == '/'):
            slashCnt+=1
        if(slashCnt == 3):
            break
        cnt += 1

    message[0] = message[0].replace("HTTP/1.1","HTTP/1.0")
    getLine = "GET " + message[0][cnt:] 
    
    message[0] = getLine
    for line in message:
        if(line[0:16] == "Proxy-Connection"):
            message.remove(line)
    message.pop()
    tmpMsg = message[:]
    userAgentChanged = changeUserAgent(message,privacy)
    for line in tmpMsg:
        line += '\n'
        data += line
        if(line[0:6] == "Host: "):
            hostName = line[6:].encode()

    filterUser = checkRestriction(hostName, restrictionEnable, targets)

    for line in userAgentChanged:
        line += '\n'
        changeUserData += line

    return data.encode(), changeUserData.encode(), hostName, filterUser

def getDir(inputData):
    message = inputData.decode("utf-8")
    message = message.split('\n')
    msg = message[0].split(' ')
    return msg[1]


def checkUser(ip,users):
    exist = False
    volume = 0
    for userIP in users:
        if userIP["IP"] == ip:
            exist  = True
            volume = userIP["volume"]
            break
    return exist, volume


def usedVolume(data):
    vol = 0
    message = data.split('\n')
    for line in message:
        if(line[0:16] == "Content-Length: "):
            vol = line[16:]
    return int(vol)

def sendMail(targets, hostName, mail):
    notify = False
    for target in targets:
        if(target["URL"] == hostName):
            notify = target["notify"]
    if(notify):       
        mailSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        writeIntoFile("mail socket created.")

        try:
            mailSocket.connect(("mail.ut.ac.ir" ,25))
            writeIntoFile("connected to mail sever.")
        except socket.error, msg:
            sys.stderr.write("[CONNECTION ERROR] in mail socket %s\n" % msg[1])
            sys.exit(1)

        writeIntoFile("Message after connection request:" + mailSocket.recv(1024).decode())

        mailSocket.send('HELO mail.ut.ac.ir\r\n'.encode())
        writeIntoFile("Message after EHLO command:" + mailSocket.recv(1024).decode())
        
        mailSocket.send("AUTH LOGIN\r\n".encode())
        writeIntoFile(mailSocket.recv(1024).decode())

        mailSocket.send("bi5zaGFrb2Vp".encode()  + '\r\n'.encode())
        writeIntoFile(mailSocket.recv(1024).decode())

        mailSocket.send("TmFzaW0zNTI1ODI=".encode()  + '\r\n'.encode())
        writeIntoFile(mailSocket.recv(1024).decode())

        mailSocket.send("MAIL FROM:<n.shakoei@ut.ac.ir>\r\n".encode())
        writeIntoFile("After MAIL FROM command: " + mailSocket.recv(1024).decode())

        mailSocket.send("RCPT TO:<n.shakoei@ut.ac.ir>\r\n".encode())
        writeIntoFile("After RCPT TO command: " + mailSocket.recv(1024).decode())

        mailSocket.send("DATA\r\n".encode())
        writeIntoFile("After DATA command: " + mailSocket.recv(1024).decode())
        
        msg = "\r\n " + mail
        mailSocket.send(msg.encode())
        mailSocket.send('\r\n.\r\n'.encode())
        writeIntoFile("Response after sending message body:"+mailSocket.recv(1024).decode())
        
        mailSocket.send("QUIT\r\n".encode())
        writeIntoFile(mailSocket.recv(1024).decode())
        
        mailSocket.close()


def checkExpiration(data):
    expired = False
    data = data.split('\n')
    curDate, expiration = "", ""
    for line in data:
        if(line[0:4] == "Date"):
            curDate = line[5:]
        elif(line[0:7] == "Expires"):
            expiration = line[8:]
    # print "curDate: ", curDate
    # print "expiration: ", expiration
    if(expiration != "" ):
        curDate = curDate.split(' ')[4] #time
        expiration = expiration.split(' ')[4] #time
        curDate = curDate.split(':')
        expiration = expiration.split(':')

        if(curDate[0] > expiration[0]): #hour
            expired = True
        elif(curDate[0] < expiration[0]): #hour
            expired = False
        else:
            if(curDate[1] > expiration[1]): #minute
                expired = True
            elif(curDate[1] < expiration[1]): #minute
                expired = False
            else:
                if(curDate[2] > expiration[2]): #seconds
                    expired = True
                else:
                    expired = False

        if(expired == True):
            writeIntoFile("cache has been expired!!!")
        else:
            writeIntoFile("cache has not been expired!!!")
    return expired


def checkInCache(directory, connection, data):
    isInCache = False
    for i in range(len(cacheDir)):
        if(directory == cacheDir[i]):
            expired = checkExpiration(cacheRes[i])
            if(expired == False):
                isInCache = True
            global totalUsed
            totalUsed += usedVolume(data)
            tmpDir = cacheDir[i]
            cacheDir.pop(i)
            cacheDir.insert(0,tmpDir)
            tmpRes = cacheRes[i]
            cacheRes.pop(i)
            cacheRes.insert(0,tmpRes)
            if(len(cacheRes[i]) > 0):
                writeIntoFile('\n' + "----------CACHE RESPONSE----------\n" + cacheRes[i]+ "--------------------")
                connection.send(cacheRes[i])
                writeIntoFile("data sent successfully from cache to browser.")
    if(isInCache):
        writeIntoFile("CACHE HIT!")
    else:
        writeIntoFile("CACHE MISS!")    
    return isInCache


def pragmaExists(data):
    pragma = False
    message = data.split('\n')
    for line in message:
        if(line[0:6] == "pragma"):
            pragma = True
        if(line[0:13] == "Cache-Control"):
            tmp = line[14:].split(',')
            for a in tmp:
                if(a == " no-cache"):
                    pragma = True
    return pragma


def contentType(data):
	cType = []
	isHtml = False
	message = data.split('\n')
	for line in message:
		if(line[0:14] == "Content-Type: "):
			cType = line[14:].split(' ')
	# 	if(line[0:8] == "Accept: "):
	# 		print "HIIIIIIIIIIIIIIIIIIIIIIIIIIIIII"
	# 		isHtml = line.find("text/html")	

	# if(isHtml):
	# 	print isHtml
	return cType


def injection(data, navBar):
	mainData = ""
	isHead,isBody = False,False
	
	msg = data.decode("utf-8","ignore")
	msg = msg.split('\n')
	for line in range(len(msg)):
		if(msg[line] == "<body>"):
			msg[line] = msg[line]+ '\n' + '<nav style="background-color: green;color:white;">' + navBar + '</nav>'+'\n'
	for line in msg:
		line += '\n'
		mainData += line

	return mainData.encode(errors='ignore')
			
	

def threaded(connection, ip, privacy, volume, restrictionEnable, targets, cacheEnable, cacheSize, isInjection, navBar):
    data, changedUserAgent, tempData, hostName, totalData,injectData = "", "", "", "", "", ""
    inCache, pragma, inject = False, True, False
    while data == "":
        data = connection.recv(10000)
    directory = getDir(data)
    writeIntoFile("DIRECTORY : " + directory)
    writeIntoFile('\n' + "----------BROWSER REQUEST----------\n" + data.decode() + "--------------------")
    if(data < 0):
        print "ERROR IN RECEIVING FROM BROWSER!"

    elif not data:
        print('Bye!')
        writeIntoFile("Bye!")

    elif(totalUsed <= int(volume)):
        writeIntoFile("totalUsed : " + str(totalUsed))
        writeIntoFile("volume : " + volume)
        print "totalUsed : ",totalUsed
        print "volume : ",volume

        mail = data

        data, changedUserAgent, hostName, filterUser = changeData(data, privacy, restrictionEnable, targets)
        if(filterUser == False):
            writeIntoFile("connecting to website...")
            print "connecting to website..."

            if('.' in hostName):
                isInCache = checkInCache(directory, connection, data)
                if(isInCache == False):
                    toServerSocket = createServerSocket(ip,hostName)

                    writeIntoFile('\n' + "----------PROXY REQUEST----------\n" + changedUserAgent.decode() + "--------------------")
                    toServerSocket.sendall(changedUserAgent)
                    
                    while True:
                        tempData  = toServerSocket.recv(10000)
                        pragma = pragmaExists(tempData)
                        if(pragma == False):
                            totalData += tempData

                        cType = contentType(tempData)
                        if(len(cType) != 0 and cType[0] == 'text/html;'):
                        	inject = True
                        elif(len(cType) != 0 and cType[0] != 'text/html;'):
                    		inject = False

                        global totalUsed
                        totalUsed += usedVolume(tempData)
                        writeIntoFile('\n' + "----------SERVER RESPONSE----------\n" + tempData + "--------------------")
                        if(len(tempData) > 0):
                        	if(inject and isInjection):
                        		tempData = injection(tempData, navBar)
                        	writeIntoFile('\n' + "----------PROXY RESPONSE----------\n" + tempData + "--------------------")
                        	connection.send(tempData)
                        	writeIntoFile("data sent successfully from proxy to browser.")
                        
                        else:
                            break
                    if(pragma == False):
                        if(len(cacheDir) == cacheSize):
                            cacheDir.pop(len(cacheDir)-1)
                            cacheRes.pop(len(cacheRes)-1)
                        cacheRes.insert(0,totalData)
                        cacheDir.insert(0,directory)
                    toServerSocket.close()
        else:
            print "you are not allowed to access this website!"
            sendMail(targets, hostName[0:len(hostName)-2], mail)
            # connection.close()
    else:
        print "[Error] your volume is empty!" 
    connection.close()
    return

def createProxySocket(ip, port, privacy, users, restrictionEnable, targets, cacheEnable, cacheSize, isInjection, navBar):
    threads = []    
    toProxySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    writeIntoFile("proxy socket created.")

    allowedUser, volume = checkUser(ip,users)
    if (allowedUser):
        try:
            toProxySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            toProxySocket.bind((ip, port))
            writeIntoFile("proxy socket binded to " + str(port) + " .")
        except socket.error, msg:
            sys.stderr.write("[BINDING ERROR] in proxy socket %s\n" % msg[1])
            sys.exit(1)

        try:
            toProxySocket.listen(5)
            writeIntoFile("proxy socket is listening...")
        except socket.error, msg:
            sys.stderr.write("[LISTENING ERROR] in proxy socket %s\n" % msg[1])
           
        while True:
            try:
                connection, addr = toProxySocket.accept()
                writeIntoFile("accept request.")
                writeIntoFile("proxy connected to : " + addr[0] + " , " + str(addr[1]) + " .")
            except socket.error, msg:
                print "error in accepting proxy socket ",msg
                
            t = Thread(target=threaded, args=(connection, ip, privacy, volume, restrictionEnable, targets, cacheEnable, cacheSize, isInjection, navBar, ))
            t.start()
            threads.append(t)
 
        for t in threads:
            t.join()
    else:
        print "you can not use proxy!"
        return


if __name__ == "__main__":
    port, isEnable, logFile, privacy, users, restrictionEnable, targets, cacheEnable, cacheSize, isInjection, navBar = parseConfig()
    print "Enter your IP : "
    ip = raw_input()
    if(isEnable):
        print "writing into log file ..."
    print users[0]["IP"]
    writeIntoFile("Proxy launched.")
    createProxySocket(ip, port, privacy, users, restrictionEnable, targets, cacheEnable, cacheSize, isInjection, navBar)